﻿function Map(svgTemplate, mapData, allParticipants, options, arena) {

    var svg = new WorldMap();
    svg = svg.svg;
    this.svg = new WorldMap();
    this.svg = this.svg.svg;

    options = typeof options !== 'undefined' ? options : {};
    mapData = typeof mapData !== 'undefined' ? mapData : {};

    var defaultOptions = {
        imageWidth: typeof options.imageWidth !== 'undefined' ? options.imageWidth : 717,
        imageHeight: typeof options.imageHeight !== 'undefined' ? options.imageHeight : 0,
        countryFill: typeof options.countryFill !== 'undefined' ? options.countryFill : 'white',
        countryStroke: typeof options.countryStroke !== 'undefined' ? options.countryStroke : 'black',
        countryStrokeWidth: typeof options.countryStrokeWidth !== 'undefined' ? options.countryStrokeWidth : 2,
        colorGenerator: typeof options.colorGenerator !== 'undefined' ? options.colorGenerator : new Colors(),
        absoluteColorOnly: typeof options,
        
        viewBoxx2: 2775,
        viewBoxy2: 1400,
        viewBoxx1: -40,
        viewBoxy1: 80,
        imageRatio: typeof options.imageRatio !== 'undefined' ? options.imageRatio : 0,
        steps: 5

    }

    defaultOptions.imageRatio = defaultOptions.viewBoxx2 / defaultOptions.viewBoxy2;
    defaultOptions.imageHeight = defaultOptions.imageWidth / defaultOptions.imageRatio;
    this.options = defaultOptions;

    var allMyParticipants = new Array();
    for (var key in allParticipants) {
        if (key != 'length') {
            allMyParticipants.push(allParticipants[key]);
        }
    }

    allMyParticipants.sort(function (a, b) {
        return b.votes - a.votes;
    });


    /**
    * Compile a list of all the countries id that appear in the map.
    *
    */
    var regions = svg.getElementsByClassName("regionxx");
    var countries = [];

    for (var i = 0; i < regions.length; i++) {
        var nextRegion = regions[i].getAttribute("id");
        countries.push(nextRegion);
    }

    this.initCountries(svg, defaultOptions);

    if (allMyParticipants.length <= 2) {
        this.createStepLegend(svg, allMyParticipants[0], allMyParticipants[1], defaultOptions.colorGenerator, defaultOptions);
    } else {

        var winners = {};
        for (var region in mapData) {
            var value = mapData[region];
            var winner = value.participants[0];
            winners[winner.char] = winner;

            for (var index in value.participants) {
                var partici = value.participants[index];
                if (partici.votes == winner.votes) {
                    winners[partici.char] = partici;
                }
            }
        }

        var winnersArray = new Array();
        
        for (var key in winners) {
            winnersArray.push(winners[key]);
        }


        if (allMyParticipants.length > 8 ){
            
        } else {
            winnersArray = allMyParticipants;
        }
       

        //winnersArray.sort(function (a, b) {
        //    if (a.char > b.char) return 1;
        //    else if (a.char < b.char) return -1;
        //    else return 0;
        //});

        //console.log(winnersArray);
        //var region = mapData[country];


        this.createSimpleLegend(svg, winnersArray, defaultOptions.colorGenerator, defaultOptions);
    }

    countries.forEach(function (country) {
        if (!(country in this.regionData)) { return };
        var region = this.regionData[country];
        var participants = this.allParticipants;

        for (var partici in region.participants) {

            
            var curr = region.participants[partici];

            curr.percentage = curr.votes / region.totalVote;
            curr.alpha = calculateAlpha(curr.percentage, defaultOptions.steps);
        }

        if (participants.length <= 2) {
            this.map.setupGradient(this.map.svg, [rawToRGBA(defaultOptions.colorGenerator.getColor(region.participants[0].char), region.participants[0].alpha)], "gradientID" + country + arena);
            this.map.mapCountryToGradient(this.map.svg, country, "gradientID" + country + arena);
        } else { 
            var winners = new Array();

            var votes = region.participants[0].votes;


            winners.push(rawToRGB(defaultOptions.colorGenerator.getColor(region.participants[0].char)));

            for (var i = 1; i < region.participants.length; i++) {
                if (region.participants[i].votes == votes) {
                    winners.push(rawToRGB(defaultOptions.colorGenerator.getColor(region.participants[i].char)));
                } else {
                    break;
                }

            }


            this.map.setupGradient(this.map.svg, winners, "gradientID" + country + arena);
            this.map.mapCountryToGradient(this.map.svg, country, "gradientID" + country + arena);
        }

    }.bind({ allParticipants: allMyParticipants, regionData: mapData, map: this }));

    
    

    defaultOptions.imageRatio = defaultOptions.viewBoxx2 / defaultOptions.viewBoxy2;
    defaultOptions.imageHeight = defaultOptions.imageWidth / defaultOptions.imageRatio;
    
    
	this.defaultOptions = defaultOptions;

	svg.setAttribute("viewBox", `${defaultOptions.viewBoxx1} ${defaultOptions.viewBoxy1} ${defaultOptions.viewBoxx2} ${defaultOptions.viewBoxy2}`);
    svg.setAttribute("width", defaultOptions.imageWidth);
    svg.setAttribute("height", defaultOptions.imageHeight);
    
    this.arena = arena;

   // this.png = function () {
        //console.log(this);
     //   return this.map.generatePng(this.map.svg, this.map.defaultOptions, this.map.arena);
    //}.bind({ map: this });

    
    this.getPngContainer = function () { return getPngContainer(defaultOptions, arena) };
}

function calculateAlpha(percentage, steps) {
    var alpha = (percentage - 0.5) / 0.5;

    alpha = Math.round(alpha * steps ) / ( steps);
    return alpha;
}

Map.prototype.initCountries = function (svg, options) {
    var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    rect.setAttribute('width', 4000);
    rect.setAttribute('height', 4000);
    rect.setAttribute('x', '-500');
    rect.style.fill = 'white';

    svg.insertBefore(rect, svg.firstChild);

    var paths = svg.getElementsByClassName('regionxx');
    for (var i = 0; i < paths.length; i++) {
        var path = paths[i];
        path.style.stroke = options.countryStroke;
        path.style.strokeWidth = options.countryStrokeWidth;
        path.setAttribute('fill', options.countryFill);
    }

    this.svg = svg;
}


Map.prototype.createStepLegend = function(svg, part1, part2, colorGenerator, options){

    var startY = options.viewBoxy2 + options.viewBoxy1 ;
    var steps = options.steps;
    
    var legendWidthRatio = 0.9;
    var fullMapWidth = (options.viewBoxx2 - options.viewBoxx1);
    var fullLegendWidth = fullMapWidth * legendWidthRatio;
    var fullBuffer = fullMapWidth - fullLegendWidth;
   
   

    var startX = options.viewBoxx1 + (fullBuffer / 2);
    var maxX = options.viewBoxx2 - (fullBuffer / 2);


    var increments = (2 * steps + 1)
    var increX = fullLegendWidth / increments;
    var increY = 100;

    var currentY = startY;
    var currentX = startX;

    options.viewBoxy2 += 225;


    var placeBox = function (width, height, y, x, color) {
        var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
        rect.setAttribute('width', width);
        rect.setAttribute('height', height);
        rect.setAttribute('y', '' + y);
        rect.setAttribute('x', x);
        rect.setAttribute('style', 'fill:' + color) + ';';
        rect.setAttribute('stroke', 'black');
        rect.setAttribute('stroke-width', '0');
        rect.setAttribute('stroke-opacity', '0.0');
        return rect;
    }

    var text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    text.setAttribute('y', '' + (currentY + 200));
    text.setAttribute('x', currentX);
    text.setAttribute('height', '25');
    text.setAttribute('font-family', 'sans-serif');
    text.setAttribute('font-weight', 'bold');
    text.setAttribute('font-size', '60px');
    text.setAttribute('fill', 'black');
    text.innerHTML = part1.char;
    svg.appendChild(text);


    var stepIncre = 1 / steps;
    var alphas = new Array();

    for (var i = 0; i < steps * 2 + 1; i++) {
        alphas.push(Math.abs(1 - (i * stepIncre)));
    }

    var i = 0;
    for (i; i < steps; i++) {
        var color = rawToRGBA(colorGenerator.getColor(part1.char), alphas[i]);
        svg.appendChild(placeBox(increX, increY, currentY, currentX, color));
        currentX += increX;
    }

    var color = rawToRGBA(colorGenerator.getColor(part1.char), alphas[i]);
    svg.appendChild(placeBox(increX, increY, currentY, currentX, color));
    currentX += increX;
    i++;

    for (i; i < increments; i++) {
        var color = rawToRGBA(colorGenerator.getColor(part2.char), Math.min(alphas[i], 1));
        svg.appendChild(placeBox(increX, increY, currentY, currentX, color));
        currentX += increX;
    }

    
    currentX -= increX;
    text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    text.setAttribute('y', '' + (currentY + 200));
    text.setAttribute('x', currentX + increX);
    text.setAttribute('height', '25');
    text.setAttribute('font-family', 'sans-serif');
    text.setAttribute('font-weight', 'bold');
    text.setAttribute('font-size', '60px');
    text.setAttribute('fill', 'black');
    text.setAttribute('text-anchor', 'end');
    text.innerHTML = part2.char;
    svg.appendChild(text);
   
}
Map.prototype.createSimpleLegend = function (svg, participants, colorGenerator, options) {

    var legendWidthRatio = 0.8;
    var fullMapWidth = (options.viewBoxx2 - options.viewBoxx1);
    var fullLegendWidth = fullMapWidth * legendWidthRatio;
    var fullBuffer = fullMapWidth - fullLegendWidth;
    var middleBuffer = .1;
    var legendSpace = legendWidthRatio - middleBuffer;

    var startX = options.viewBoxx1 + (fullBuffer / 2);// 10% of the width should be whitespace
    var maxX = options.viewBoxx2;

    var startY = options.viewBoxy2 + options.viewBoxy1;

    var maxX = options.viewBoxx2;
    var increX = fullLegendWidth * (legendSpace / 2);
    var increY = 120;

    var currentY = startY;
    var currentX = startX;
    options.viewBoxy2 += increY;

    for (var key = 0; key < participants.length; key++) {
      

        if (currentX + increX  > maxX) {
            currentX = startX;
            currentY += increY;
            options.viewBoxy2 += increY;
        } 

        var entry = participants[key].char;
        var color = colorGenerator.getColor(entry);
        color = rawToRGB(color);

        var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
        rect.setAttribute('width', '100');
        rect.setAttribute('height', '100');
        rect.setAttribute('y', '' + currentY);
        rect.setAttribute('x', currentX);
        rect.setAttribute('style', 'fill:' + color) + ';';
        rect.setAttribute('stroke', 'black');
        rect.setAttribute('stroke-width', '1');
        svg.appendChild(rect);

        var text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
        text.setAttribute('y', '' + (currentY + 73));
        text.setAttribute('x', currentX + 10 + 100);
        text.setAttribute('height', '25');
        text.setAttribute('font-family', 'sans-serif');
        text.setAttribute('font-weight', 'bold');
        text.setAttribute('font-size', '60px');
        text.setAttribute('fill', 'black');
        text.innerHTML = entry;
        svg.appendChild(text);
    
        currentX += increX;        
    }
}

Map.prototype.setupGradient = function (svg, colors, gradientID) {

    var gradientStripSize = '14';
    
    var defs = document.createElementNS("http://www.w3.org/2000/svg", 'defs');

    var pattern = document.createElementNS("http://www.w3.org/2000/svg", "pattern");
    pattern.setAttribute('id', gradientID);
    pattern.setAttribute('x', "0");
    pattern.setAttribute('y', "0");
    pattern.setAttribute('width', "10px");
    pattern.setAttribute('height', "" + colors.length * gradientStripSize + "px");
    pattern.setAttribute('patternUnits', "userSpaceOnUse");

    for (var i = 0; i < colors.length; i++) {
        var thisColor = colors[i];
        var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        rect.setAttribute("width", "10px");
        rect.setAttribute("height", "" + gradientStripSize + "px");
        rect.setAttribute("y", "" + (i * gradientStripSize) );
        rect.setAttribute("style", "stroke: none; stroke-width: 0px; fill: " + thisColor);
        pattern.appendChild(rect);
    }

    defs.appendChild(pattern);
   
    svg.appendChild(defs);
}

function rawToRGB(colorCoord) {
    //return 'rgb(' + colorCoord[0] + ',' + colorCoord[1] + ',' + colorCoord[2] + ')';
    return rawToRGBA(colorCoord, 0.8);
}

function rawToRGBA(colorCoord, alpha){
    return 'rgba(' + colorCoord[0] + ',' + colorCoord[1] + ',' + colorCoord[2] + ',' + alpha + ')';
}

Map.prototype.mapCountryToGradient = function (svg, countryID, gradientID) {
    var myCountry = svg.getElementById('' + countryID);
    myCountry.setAttribute('fill', 'url(#' + gradientID + ')');

    return svg;
}

function generatePng (svg, options, id) {
    var svgHeader = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   height="${options.imageHeight}"
   version="1.1"
   width="${options.imageWidth}"
   viewBox="${options.viewBoxx1} ${options.viewBoxy1} ${options.viewBoxx2} ${options.viewBoxy2} "
   id="svg3336"
   sodipodi:docname="type2.svg">

 " />`

    $(".mapCanvas").remove(".mapCanvas");
    var svgString = svgHeader + svg.innerHTML + '</svg>';

    //console.log(svgString);

    var canvas = document.createElement('canvas');
    canvas.width = options.imageWidth;
    canvas.height = options.imageHeight;

    var pngContainer = document.createElement('img');
    pngContainer.height = canvas.height;
    pngContainer.width = canvas.width;
    pngContainer.className = "canvas map";
    pngContainer.id = id;

    var ctx = canvas.getContext('2d');
    var DOMURL = self.URL || self.webkitURL || self;
    var img = new Image();
    console.log(svg);
    var svg3 = new Blob([svgString], { type: "image/svg+xml;charset=utf-8" });
    var url = DOMURL.createObjectURL(svg3);
    img.onload = function () {
        
        ctx.drawImage(this.img, 0, 0);
        var png = canvas.toDataURL("image/png");
        var pngContainer = this.pngContainer;
        pngContainer.src = png;
        DOMURL.revokeObjectURL(png);
    }.bind({ id: id, pngContainer: pngContainer, img: img });

 
    img.src = url;

    return pngContainer;
}

function getPngContainer(options, id) {
    var pngContainer = document.createElement('img');
    pngContainer.height = options.imageHeight;
    pngContainer.width = options.imageWidth;
    pngContainer.className = "canvas map";
    pngContainer.id = id;

    return pngContainer;
}

function generateMaps() {

    var storage = this;
    $(".mapContainer").remove(".mapContainer");

    var data = storage.xlsx.consolidatedResults;
    
    var svgs = new Array();
    for (var arena in data) {

        convertRegionKeys(data[arena]);

        var regionGrouping = getParticipantsDataByRegion(data[arena]);

        for (var region in regionGrouping) {
            var regionData = regionGrouping[region];
        }

        var allParticipants = getAllParticipants(data[arena]);
        storage.colorGenerator = new Colors();
        
        var map = new Map(storage.svg, regionGrouping, allParticipants, storage, "mapDrawing" + arena);
        svgs.push([arena, map]);
        
    }

    for (var i = 0; i < svgs.length; i++) {

        var arena = svgs[i][0];
        var map = svgs[i][1];

        var mapDiv = document.createElement('div');
        var title = document.createElement("h2");
        var text = document.createTextNode("Arena " + arena);
        title.appendChild(text);
        mapDiv.appendChild(title);


        mapDiv.appendChild(generatePng(map.svg, map.options, arena));
        //mapDiv.appendChild(map.getPngContainer());
        //mapDiv.appendChild(map.svg);
        mapDiv.className = "mapContainer";
        document.body.appendChild(mapDiv);
    }

    

    //map.png();
    document.getElementById("exportimages").disabled = false;
}

function convertRegionKeys(data) {
    var regionMapper = new RegionMap();

    data.forEach(function (e) {
        e.region = regionMapper.getRegion(e.country);
    });
    
    return data;
}

function getParticipantsDataByRegion(data) {
    
    var regions = {};
    data.forEach(function (e) {
        var region = e.region;

        var regionData = typeof regions[region] !== 'undefined' ? regions[region] : { totalVote: 0, name: region};
        regions[region] = regionData;

        var participants = typeof regionData.participants !== 'undefined' ? regionData.participants : { };
        regionData.participants = participants;

        var character = typeof participants[e.char] !== 'undefined' ? participants[e.char] : {char: e.char, votes : 0};
        participants[e.char] = character;

        character.votes += 1;
        regionData.totalVote += 1;
    });

    for (var reg in regions) {
        var curr = regions[reg];
        curr.participants = $.map(curr.participants, function (value, object) {
            return [value];
        });
        curr.participants.sort(function (a, b) { return b.votes - a.votes });
    }

    console.log(data);
    return regions;
    
}

function getAllParticipants(worldData) {

    var participants = {};

    worldData.forEach(function (row) {
        var partici = typeof participants[row.char] !== 'undefined' ? participants[row.char] : {char : row.char, votes: 0};
        participants[row.char] = partici;

        partici.votes += 1;
    });

    var length = 0;
    for (var partici in participants) {
        length += 1;
    }

    participants.length = length;

    return participants;
}
