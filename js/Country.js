﻿function Country(id) {
    this.id = id;
    this.characters = {};

    this.addCharacterVote = function (char) {
        if (char in this.characters){
            this.characters[char]++;
        } else {
            this.characters[char] = 1;
        }
    }

    this.getWinners = function () {

        var winners = new Set();
        var highestVote = 0;

        for (var char in this.characters) {
            var voteCount = this.characters[char];
            if (highestVote < voteCount) {
                highestVote = this.characters[char];
                winners = new Set();
                winners.add(char);
            } else if (highestVote == voteCount) {
                winners.add(char);
            }
        }
        return Array.from(winners);
    }

    this.getVoteShare = function(character){
        var totalVoteCount = this.getTotalVote();
        var percentage = this.characters[character] / totalVoteCount;
        return percentage;
    }

    this.getTotalVote = function () {

        var totalVoteCount = 0;
        for (var char in this.characters) {
            var voteCount = this.characters[char];
            totalVoteCount += voteCount;
        }

        return totalVoteCount;
    }
}

