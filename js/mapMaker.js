﻿function Character(name) {
    this.name = name;
    this.color = randomColor();
}

function Country(id) {
    this.id = id;
    this.characters = {};
    this.winners = [];

    this.addCharacter = function (char) {
        this.characters[char.name] = {
            char: char,
            votes: 0
        }
    }

    this.addVote = function (char, incr) {
        if (char.name in this.characters) {
            this.characters[char.name]['votes'] += incr;
        } else {
            this.addCharacter(char);
            this.addVote(char, incr);
        }
    }

    this.getWinners = function () {

        if (this.winners.length == 0) {
            var maxVote = 0;
            for (char in this.characters) {
                var data = this.characters[char];
                if (data['votes'] > maxVote) {
                    maxVote = data['votes'];
                }
            }

            var bestVoteChar = [];
            for (char in this.characters) {
                var data = this.characters[char];
                if (data['votes'] == maxVote) {
                    bestVoteChar.push(data)
                }
            }

            this.winners = bestVoteChar;
        }
        return this.winners;
    }

}

function randomColor() {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
}

function MapData() {
    this.processXLSX = function (payload) {
        var data = payload.target.result;
        var workbook = XLSX.read(data, { type: 'binary' });
        var results = toJson(workbook);
        payload.target.results = results;
        payload.target.mapData.data = data["Sheet4"];

        var mapData = new MapData();

        console.log(mapData);
        mapData.data = results["Sheet4"];
        mapData.processCountries();
        mapData.vectorMap = this.mapData.vectorMap;

        var colors = ['blue', 'red', 'green', 'yellow', 'purple', 'crimson', 'brown', 'cyan', 'darksalmon', 'deeppink'];
        var i = 0;
        var charHistory = {};
        for (var country in mapData.countryData) {
            var winners = mapData.countryData[country].getWinners();

            for (var winner in winners) {
                var char = winners[winner].char;

                if (!(char.name in charHistory)) {
                    charHistory[char.name] = colors[i++];
                    colors.push(randomColor());
                }
            }

        }

        for (var char in charHistory) {
            mapData.characterData[char].color = charHistory[char];
        }

        var svg = document.getElementById(mapData.vectorMap).getElementsByTagName('svg')[0];
        var paths = svg.getElementsByTagName('g');

        // Fills the country with grey and sets the borders to a black
        for (var i = 0; i < paths.length; i++) {
            var path = paths[i];
            path.style.stroke = 'black';
            path.style.strokeWidth = '3';
            path.setAttribute('fill', 'grey');
        }

        mapData.editMap();
        mapData.createLegend();
        mapData.drawCanvas();
    }

    this.createLegend = function () {

        var countryData = this.countryData;
        var svg = document.getElementById('vmap').getElementsByTagName('svg')[0];

        var y = 1500;

        var charHistory = new Set();
        var characterData = this.characterData;
        var lastGX = 100;
        for (var country in countryData) {


            var winners = countryData[country].getWinners();
            for (var i = 0; i < winners.length; i++) {
                var winner = winners[i];

                if (!(charHistory.has(winner.char.name))) {

                    //var outrect = document.createElementNS("http://www.w3.org/2000/svg", 'g');
                    var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
                    rect.setAttribute('width', '100');
                    rect.setAttribute('height', '100');
                    rect.setAttribute('y', '' + y);
                    rect.setAttribute('x', lastGX + 5);
                    rect.setAttribute('style', 'fill:' + this.characterData[winner.char.name].color) + ';';
                    rect.setAttribute('stroke', 'black');
                    rect.setAttribute('stroke-width', '1');
                    svg.appendChild(rect);

                    var text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
                    text.setAttribute('y', '' + (y + 75));
                    text.setAttribute('x', lastGX + 5 + 100);
                    text.setAttribute('height', '25');
                    text.setAttribute('font-family', 'sans-serif');
                    text.setAttribute('font-weight', 'bold');
                    text.setAttribute('font-size', '60px');
                    text.setAttribute('fill', 'black');
                    text.innerHTML = winner.char.name;
                    svg.appendChild(text);
                    //svg.appendChild(outrect);
                    if (lastGX + 200 + 200 > 2500) {
                        lastGX = 100;
                        y += 120;
                    } else {
                        lastGX += 700;
                    }
                    //y += 30;
                    charHistory.add(winner.char.name);
                    //console.log(winner.char.name + ' ' + winner.char.color);
                }

            }
        }




    }

    this.editMap = function () {


        this.setupGradient(this.countryData['us'], this.countryData['us'].getWinners(), '#vmap');


        for (var myCountry in this.countryData) {
            var country = this.countryData[myCountry];
            this.setupGradient(country, country.getWinners(), '#vmap');
        }
    }

    this.gradientIDNumber = 0;

    this.setupGradient = function (country, characters, vmap) {
        var svg = document.getElementById('vmap').getElementsByTagName('svg')[0];
        var defs = document.createElementNS("http://www.w3.org/2000/svg", 'defs');
        var linearGradient = document.createElementNS("http://www.w3.org/2000/svg", 'linearGradient');
        var gradientID = 'Gradient' + this.gradientIDNumber++;
        linearGradient.setAttribute('id', gradientID);
        linearGradient.setAttribute('x1', '0');
        linearGradient.setAttribute('x2', '0');
        linearGradient.setAttribute('y1', '0');
        linearGradient.setAttribute('y2', '.1');
        linearGradient.setAttribute('spreadMethod', 'repeat');

        for (var i = 0; i < characters.length; i++) {

            var thisChar = characters[i].char;


            var stop = document.createElementNS("http://www.w3.org/2000/svg", 'stop');
            stop.setAttribute('stop-color', thisChar.color);
            stop.setAttribute('offset', "" + ((100 / characters.length) * i) + "%");
            linearGradient.appendChild(stop);

            stop = document.createElementNS("http://www.w3.org/2000/svg", 'stop');
            stop.setAttribute('stop-color', thisChar.color);
            stop.setAttribute('offset', "" + ((100 / characters.length) * (i + 1)) + "%");
            linearGradient.appendChild(stop);
        }

        defs.appendChild(linearGradient);
        svg.appendChild(defs);

        var myCountry = document.getElementById('' + country.id);
        myCountry.setAttribute('fill', 'url(#' + gradientID + ')');
    }

    this.processCountries = function () {

        var data = this.data;
        var countries = {};
        var characters = {};
        var regionMapper = new RegionMap();

        var i = 0;
        var colors = ['blue', 'red', 'green', 'yellow', 'purple', 'crimson', 'brown', 'cyan', 'darksalmon', 'deeppink'];


        for (var j = 0; j < data.length; j++) {

            var e = data[j];

            var country = regionMapper.getRegion(e["Country"]);
            var chars = new Set();
            chars.add(e["char1"]);
            chars.add(e["char2"]);
            chars.add(e["char3"]);
            chars.add(e["char4"]);
            chars.add(e["char5"]);

            var myCountry;
            if (!(country in countries)) {
                countries[country] = new Country(country);
            }
            myCountry = countries[country];

            chars.forEach(function (char) {
                var myChar;
                if (!(char in characters)) {
                    characters[char] = new Character(char);
                    characters[char].color = colors[i++];
                    //colors.splice(0, 1);
                    colors.push(randomColor());

                }
                myChar = characters[char];
                myCountry.addVote(myChar, 1);

                // console.log(myChar.color);
            });



        }

        this.countryData = countries;
        this.characterData = characters;


    }

}

function buildXLSX(path) {
    var files = path.target.files;
    var i, f;
    console.log(this);

    xhr = new XMLHttpRequest();
    xhr.open("GET", "World Map-ver09.svg", true);
    // Following line is just to be on the safe side;
    // not needed if your server delivers SVG with correct MIME type
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var svg = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
            svg.appendChild(xhr.responseXML.documentElement);

            var mapData = new MapData();
            mapData.svg = svg;
            if (true) {
                mapData.path = path;
                mapData.vectorMap = 'vmap';

                var workbooks = new Array();

                for (i = 0, f = files[i]; i != files.length; i++) {
                    var reader = new FileReader();
                    var name = f.name;

                    reader.mapData = mapData;

                    reader.onload = function (payload) {
                        var data = payload.target.result;
                        var workbook = XLSX.read(data, { type: 'binary' });
                        var results = toJson(workbook);
                        console.log(results);
                        if (false) {
                            payload.target.results = results;
                            payload.target.mapData.data = data["Sheet4"];

                            var mapData = new MapData();

                            console.log(mapData);
                            mapData.data = results["Sheet4"];
                            mapData.processCountries(results["Sheet4"]);
                            mapData.vectorMap = this.mapData.vectorMap;

                            var colors = ['blue', 'red', 'green', 'yellow', 'purple', 'crimson', 'brown', 'cyan', 'darksalmon', 'deeppink'];
                            var i = 0;
                            var charHistory = {};
                            for (var country in mapData.countryData) {
                                var winners = mapData.countryData[country].getWinners();

                                for (var winner in winners) {
                                    var char = winners[winner].char;

                                    if (!(char.name in charHistory)) {
                                        charHistory[char.name] = colors[i++];
                                        colors.push(randomColor());
                                    }
                                }

                            }

                            for (var char in charHistory) {
                                mapData.characterData[char].color = charHistory[char];
                            }

                            var svg = document.getElementById(mapData.vectorMap).getElementsByTagName('svg')[0];
                            var paths = svg.getElementsByTagName('g');

                            // Fills the country with grey and sets the borders to a black
                            for (var i = 0; i < paths.length; i++) {
                                var path = paths[i];
                                path.style.stroke = 'black';
                                path.style.strokeWidth = '3';
                                path.setAttribute('fill', 'grey');
                            }

                            mapData.editMap();
                            mapData.createLegend();
                            mapData.drawCanvas();
                        }

                        
                    }
                    reader.readAsBinaryString(f);
                }
            }
        }
    };
    xhr.overrideMimeType("image/svg+xml");
    xhr.send("");
}


function readerOnLoad(e) {
    var data = e.target.result;
    var workbook = XLSX.read(data, { type: 'binary' });
    var results = toJson(workbook);
    e.target.results = results;
    e.target.mapData = results;
    this.results = results;
    e.target.afterwords();
}

function toJson(workbook) {
    var results = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            results[sheetName] = roa;
        }
    });
    return results;
}