﻿var Colors = function () {
    this.baseColors = [[114, 147, 203], [225, 151, 76], [132, 186, 91], [211, 94, 96], [128, 133, 133], [144, 103, 167], [171, 104, 87], [204, 194, 16], ];
    this.index = 0;

    this.history = {};

    this.getColor = function (character) {

        if ((character in this.history)) return this.history[character];


        this.history[character] = this.baseColors[(this.index++)%this.baseColors.length];
        return this.history[character];
    }
}