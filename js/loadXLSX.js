﻿


function xlsxParser() {
    this.mybook = {};

    this.readerOnLoad = function (e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, { type: 'binary' });
        var results = to_json(workbook);
        //this.mybook["workbook"] = results;
        e.target.results = results;

    };
    
    this.loadFile = function (path, callback) {
        this.path = path;
        var files = path.target.files;
        var i, f;

        var workbooks = new Array();
        
        for (i = 0, f = files[i]; i != files.length; i++) {
            console.log(i);
            var reader = new FileReader();
            var name = f.name;

            reader.onload = callback;

            reader.readAsBinaryString(f);

        }
    };
}


function buildXLSX(xlsx) {
    var parser = new xlsxParser();
    parser.loadFile(xlsx, function (e) {
        var data = e.target.result;
        var workbook = XLSX.read(data, { type: 'binary' });
        var results = to_json(workbook);
        e.target.results = results;
        //console.log(JSON.stringify(results));

        count_countries(results["Sheet4"]);
    });
}

function count_countries(worksheet) {
    //console.log(JSON.stringify(worksheet));

    var countries = [];

    worksheet.forEach(function (e) {

        var regionMapper = new RegionMap();



        var country = regionMapper.getRegion(e["Country"]);
        //console.log(country);
        var chars = [];
        chars.push(e["char1"]);
        chars.push(e["char2"]);
        chars.push(e["char3"]);
        chars.push(e["char4"]);
        chars.push(e["char5"]);

        if (!(country in countries)) {
            countries[country] = {};
        }

        chars.forEach(function (f) {
            if (!(f in countries[country])) {
                countries[country][f] = 0
            }
            countries[country][f] = countries[country][f] + 1;
        });

       // console.log(country + " " + JSON.stringify(countries[country]));
    });
    var mapData = findMax(countries);
    contestantColors(mapData);

    var colorData = getRegionColor(mapData);
    
    jQuery('#vmap').vectorMap(
            {
                map: 'isml_map',
                backgroundColor: '#a5bfdd',
                borderColor: '#000000',
                borderOpacity: 0.5,
                borderWidth: 0,
                colors: colorData,
                color: '#ffffff',
                enableZoom: false,
                // hoverColor: '#c9dfaf',
                //hoverOpacity: null,
                normalizeFunction: 'linear',
                //scaleColors: ['#b6d6ff', '#005ace'],
                //selectedColor: '#c9dfaf',
                selectedRegions: [],
                showTooltip: false,

            });

    var svg = document.getElementById('vmap').getElementsByTagName('svg')[0];
    var defs = document.createElementNS("http://www.w3.org/2000/svg", 'defs');
    var linearGradient = document.createElementNS("http://www.w3.org/2000/svg", 'linearGradient');
    linearGradient.setAttribute('id', 'Gradient1');
    linearGradient.setAttribute('x1', '0');
    linearGradient.setAttribute('x2', '0');
    linearGradient.setAttribute('y1', '0');
    linearGradient.setAttribute('y2', '.1');
    linearGradient.setAttribute('spreadMethod', 'repeat');

    var stop = document.createElementNS("http://www.w3.org/2000/svg", 'stop');
    stop.setAttribute('stop-color', 'red');
    stop.setAttribute('offset', '50%');
    linearGradient.appendChild(stop);



    stop = document.createElementNS("http://www.w3.org/2000/svg", 'stop');
    stop.setAttribute('stop-color', 'blue');
    stop.setAttribute('offset', '50%');
    linearGradient.appendChild(stop);

    defs.appendChild(linearGradient);

    svg.appendChild(defs);

    var us = document.getElementById('jqvmap1_us');
    us.setAttribute('fill', 'url(#Gradient1)');

    var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    rect.setAttribute('width', 100);
    rect.setAttribute('height', 100);
    rect.setAttribute('fill', '#000000');
    rect.setAttribute('x', '1000');
    rect.setAttribute('y', '1300');

    var text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    text.setAttribute('x', 1100);
    text.setAttribute('y', 1355);
    text.setAttribute('font-size', 50);
    text.innerHTML = "Tomori Nao";

    var g = svg.getElementsByTagName('g')[0];
    g.appendChild(rect);
    g.appendChild(text);

    console.log(svg);
}

function getRegionColor(countryList) {
    var colorList = {};
    countryList.forEach(function (e) {
        colorList[e['country']] = e['color'];
    });
    return colorList;
}
function contestantColors(contestantList) {
    var contestants = {}
    contestantList.forEach(function (e) {
        var contestant = e["contestant"];
        if (!(contestant in contestants)){
            contestants[contestant] = randomColor();
        }
        e["color"] = contestants[contestant];
    });
}
function findMax(countries) {
    var maxCountries = [];
    for (var country in countries) {
        var votes = countries[country];

        var max = { "character": "", "votes": 0 };

        for (var girl in votes) {
            var voteCount = votes[girl];

            if (max["votes"] < voteCount) {
                max["character"] = girl;
                max["votes"] = voteCount;
            }
        }
        maxCountries.push({ "country": country, "contestant": max["character"], "votes": max["votes"] });

    }

    return maxCountries;

}

function randomColor() {
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
}

function printFirstFiveRows(workbooks, id) {
    var output = document.getElementById(id);
    var string = "";
    var count = 0;
    workbooks.forEach(function (e) {
        var thisBook = e["workbook"];
        
        for (worksheet in thisBook) {
            string += "<h1>" + worksheet + "</h1>";
            var rows = thisBook[worksheet];
            
            for (row in rows) {
                if (count == 0) {
                    var headers = Object.keys(row);
                    string += "<b>" + headers.join(",") + "</b>";
                    count++;
                }

                string += "<p>";
                var values = new Array();
                for (element in row) {
                    values.push(row[element]);
                }

                string += values.join(",") + "</p>";

            }

            
        }
    });
    output.innerHTML = string;
}

function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}