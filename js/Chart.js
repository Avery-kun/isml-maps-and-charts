﻿function generateCharts() {

    var storage = this;
    $(".chartContainer").remove(".chartContainer");

    var data = storage.xlsx.consolidatedResults;
    for (var arena in data) {

        google.charts.setOnLoadCallback(function () {
            var currentData = processDate(data[arena]);

            var dateArray = new Array();

            currentData.forEach(function (e) {
                dateArray.push(new DateNode(e));
            });

            dateArray.sort(function (a, b) {
                return a.date.valueOf() - b.date.valueOf();
            });

            var chars = groupCharacter(dateArray);

            var charsByDate = countByDate(chars);

            var minDate = Number.MAX_SAFE_INTEGER;
            var maxDate = 0;

            for (var char in charsByDate) {
                var firstDate = charsByDate[char][0][0];
                if (minDate > firstDate) {
                    minDate = firstDate;
                }

                var lastDate = charsByDate[char][charsByDate[char].length - 1][0];
                if (maxDate < lastDate) {
                    maxDate = lastDate;
                }
            }

            for (var char in charsByDate) {
                if (minDate < charsByDate[char][0][0]) {
                    charsByDate[char].splice(0, 0, [minDate, 0]);
                }
                if (maxDate > charsByDate[char][charsByDate[char].length - 1][0]) {
                    charsByDate[char].push([maxDate, 0]);
                }
            }

            for (var char in charsByDate) {
                charsByDate[char] = fillMissingHours(charsByDate[char]);
            }

            var charChartData = {};

            for (var char in charsByDate) {
                var value = charsByDate[char];

                charChartData[char] = { header: new Array(), values: new Array() };
                charChartData[char].header.push(0);
                charChartData[char].values.push(0);
                var cumValue = 0;
                var hour = 1;
                value.forEach(function (e) {
                    cumValue += e[1];
                    charChartData[char].header.push(hour++);
                    charChartData[char].values.push(cumValue);
                });
            }

            var header = [];
            for (var char in charChartData) {
                header = charChartData[char].header;
            }


            var chartData = new Array();
            var ticks = new Array();

            var totalTicks = 12;
            var targetTicks = Math.round(header.length / totalTicks);
            header.forEach(function (e) {
                var array = new Array();
                array.push(e);
                chartData.push(array);

                if (e % targetTicks == 0) {
                    ticks.push(e);
                }
            });

            for (var char in charChartData) {
                for (var i = 0; i < charChartData[char].values.length; i++) {
                    chartData[i].push(charChartData[char].values[i]);
                }
            }

            var characterList = new Array();

            var chartData2 = new google.visualization.DataTable();
            chartData2.addColumn('number', 'Hour');

            for (var char in charChartData) {
                chartData2.addColumn('number', char);
            }

            chartData2.addRows(chartData);

            var options = {
                title: "Arena " + arena,
                curveType: "function",
                legend: { position: 'top', maxLines: 8 },
                width: storage.imageWidth,
                height: storage.imageWidth / 1.814,
                hAxis: { ticks: ticks },
                vAxis: { viewWindow: { min: 0 } },
            };


            console.log(chartData);


            var div = document.createElement('div');
            var title = document.createElement('h2');
            var text = document.createTextNode("Arena " + arena);
            title.appendChild(text);
            div.appendChild(title);

            div.className = "chartContainer";
            div.setAttribute('id', 'chart' + arena);
            document.body.appendChild(div);

            var chart_div = document.getElementById('chart' + arena);

            var chart = new google.visualization.LineChart(chart_div);
            //var chart = new google.charts.Line(chart_div);

            google.visualization.events.addListener(chart, 'ready', function () {
                chart_div.innerHTML = '<img class="canvas chart" id="chartpng' + arena + '" src="' + chart.getImageURI() + '">';
            });
            chart.draw(chartData2, options);
        });
        
        document.getElementById("exportimages").disabled = false;
    }
}

function fillMissingHours(countDateArray) {
    var hour = 60 * 60 * 1000; //utime one hour

    var currHour = countDateArray[0][0];

    var count = 1;
    while (count < countDateArray.length) {
        currHour += hour;
        if (currHour < countDateArray[count][0]) {
            countDateArray.push([currHour, 0]);
        } else {
            count++;
        }
    }

    countDateArray.sort(function (a, b) {
        return a[0] - b[0];
    });
    return countDateArray;
}

function countByDate(chars) {

    var charObj = {};

    for (var name in chars) {
        charObj[name] = new Array();

        var charCounts = charObj[name];
        var data = chars[name];

        var count = 0;
        var currDate = null;

        data.forEach(function (e) {
            if (e.date.valueOf() == currDate) {
                count++;
            } else {
                if (currDate != null) {
                    charCounts.push([currDate, count]);
                }
                currDate = e.date.valueOf();
                count = 1;

            }
        });

        charCounts.push([currDate, count]);
    }

    return charObj;
}

function groupCharacter(dateArray) {
    var chars = {};

    dateArray.forEach(function (e) {
        var currChar = typeof chars[e.data.char] !== 'undefined' ? chars[e.data.char] : new Array();
        chars[e.data.char] = currChar;

        currChar.push(e);
    });

    return chars;
}

function processDate(data) {
    data.forEach(function (e) {
        e.date = roundDateHour(new Date(e.utime * 1000));
    });

    return data;
}

function groupByHour(data) {

    var hourGroup = {};
    data.forEach(function (e) {
        var hour = e.date.getUTCHours();
        var myGroup = typeof hourGroup[hour] !== 'undefined' ? hourGroup[hour] : new Array();
        hourGroup[hour] = myGroup;

        myGroup.add(e);
    });

    return hourGroup;
}

function roundDate(date) {
    var roundedDate = new Date(date.toUTCString());

    roundedDate.setSeconds(0);
    roundedDate.setMinutes(0);
    roundedDate.setMilliseconds(0);
    roundedDate.setHours(0);

    return roundedDate;
}

function roundDateHour(date) {
    var roundedDate = new Date(date.toUTCString());

    roundedDate.setSeconds(0);
    roundedDate.setMinutes(0);
    roundedDate.setMilliseconds(0);

    return roundedDate;
}

function DateNode(node) {
    this.data = node;
    this.date = roundDateHour(new Date(node.utime * 1000));
}


function groupByDate(data) {


}