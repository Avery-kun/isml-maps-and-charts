﻿function XLSXProcessor(xlsx) {
    this.xlsx = xlsx;
    this.results = this.toJson(this.xlsx);
    this.consolidatedResults = this.consolidatedSheet(this.results);
    this.configedNames = this.nameConfiguration(this.results);
}

XLSXProcessor.prototype.toJson = function (xlsx) {
    var results = {};

    xlsx.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(xlsx.Sheets[sheetName]);
        if (roa.length > 0) {
            results[sheetName] = roa;
        }
    });
    return results;
}

XLSXProcessor.prototype.nameConfiguration = function (results) {
    var workbook = results;

}

XLSXProcessor.prototype.consolidatedSheet = function (results) {

    var workbook = results;
    var arenas = {};
    var rows = new Array();

    for (var worksheet in workbook) {




        for (var j = 0; j < workbook[worksheet].length; j++) {
            var thisRow = workbook[worksheet][j];

            var ip, idate, itime, utime, ihash, gen, age, vcflag, country, char;
            ip = idate = itime = utime = ihash = gen = age = vcflag = country = char = false;
            var charCount = 0;
            

            for (var header in thisRow) {
                header = header.toLowerCase();
                ip = (header == 'ip') || ip;
                idate = (header == 'idate') || idate;
                itime = (header == 'itime') || itime;
                utime = (header == 'utime') || utime;
                ihash = (header == 'ihash') || ihash;
                char = (header.startsWith('char')) || char;
                gen = (header == 'gen') || gen;
                age = (header == 'age') || age;
                vcflag = (header == 'vc flag') || vcflag;
                country = (header == 'country') || country;
            }

            if (ip && idate && itime && utime && ihash && gen && age && vcflag && country && char) {
                var characterHeads = Array();
                for (var header in thisRow) {
                    if (header.startsWith('char')) {
                        charCount++;
                        characterHeads.push(header.split('char')[1]);
                    }
                }

                for (var charIndex in characterHeads) {
                    var votingRow = new VotingRow();
                    var row = thisRow;

                    charIndex = characterHeads[charIndex];
                    votingRow.ip = row.ip;
                    votingRow.idate = row.idate;
                    votingRow.itime = row.itime;
                    votingRow.utime = row.utime;
                    votingRow.ihash = row.ihash;
                    votingRow.gen = row.gen;
                    votingRow.age = row.age
                    votingRow.vcflag = row["VC Flag"];
                    votingRow.country = row["Country"];
                    votingRow.char = row["char" + charIndex];
                    votingRow.arena = charIndex;

                    if (typeof votingRow.char !== 'undefined' && votingRow.char != 'ABSTAIN') {

                        arenas[charIndex] = typeof arenas[charIndex] !== 'undefined' ? arenas[charIndex] : new Array();
                        arenas[charIndex].push(votingRow);
                    }
                }
                
            }
        }
    }

    return arenas;
}

function processXLSX(path) {
    var svg = this.svg;

    var files = path.target.files;
    var i, f;

    var reader = new FileReader();
    var xlsx = null;

    reader.onload = function (payload) {
        var data = payload.target.result;
        var workbook = XLSX.read(data, { type: 'binary' });
        xlsx = new XLSXProcessor(workbook);   
        document.getElementById("generateMap").disabled = false;
        document.getElementById("generateChart").disabled = false;
        this.storage.xlsx = xlsx;
    }.bind({ storage: this, reader: reader });

    reader.readAsBinaryString(files[0]);

}
