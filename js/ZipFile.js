﻿function generateZip() {

    var classes = ["canvas"];

    var nodes = [];

    classes.forEach(function (e) {
        var canvases = $("." + e);

        for (var j = 0; j < canvases.length; j++) {
            var canvas = canvases[j];
            var iii = canvas.className.indexOf('canvas ') + 7;
            canvas.filePrefix = canvas.className.substr(canvas.className.indexOf('canvas ') + 7);
            nodes.push(canvas);
        }
    });

    
    if (nodes.length == 0) return;
    
    var zip = new JSZip();

    for (var i = 0; i < nodes.length; i++) {
        var e = nodes[i];
        zip.file(e.filePrefix + "_" + e.id + ".png", e.src.substr(e.src.indexOf(',') + 1), { base64: true });
    }

    var download = document.createElement('a');
    download.href = "data:application/zip;base64," + zip.generate({ type: "base64", compression: "DEFLATE" });
    download.download = 'isml_maps.zip';
    download.click();
}