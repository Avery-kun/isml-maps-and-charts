﻿function MapProcessor(data, vcflags) {

    vcflags = typeof vcflags !== 'undefined' ? vcflags : ["0"];
    this.data = data;


    this.countryData = this.createCountries(this.data, vcflags);
    this.allWinners = this.findAllWinners(this.countryData);
    this.winnersByCountry = this.findWinnersByCountry(this.countryData);
}

MapProcessor.prototype.createCountries = function (data, vcflags) {

    var regionMapper = new RegionMap();

    var countriesData = {};

    data.forEach(function (row) {
        var answer = $.inArray(row.vcflag, vcflags)
        if ($.inArray(row.vcflag, vcflags) < 0) return;

        var char = row.char;
        var countryID = regionMapper.getRegion(row.country);

        if (countryID != null) {

            var country = countriesData[countryID];

            if (typeof country === 'undefined') {
                country = new Country(countryID);
            }

            country.addCharacterVote(char);

            countriesData[countryID] = country;
        }
    });

    return countriesData;
}

MapProcessor.prototype.findAllWinners = function (countryData) {
    countryData = typeof countryData !== 'undefined' ? countryData : {};

    var allWinners = new Set();
    for (var countryID in countryData) {
        var country = countryData[countryID];
        var winners = country.getWinners();

        winners.forEach(function (winner) {
            allWinners.add(winner);
        });
    }

    return Array.from(allWinners);
}


MapProcessor.prototype.findWinnersByCountry = function (countryData) {
    countryData = typeof countryData !== 'undefined' ? countryData : {};

    var allCountries = {};

    for (var countryID in countryData) {
        var country = countryData[countryID];
        var winners = country.getWinners();

        var winnerShares = new Array();

        for (var winner in winners) {
            winnerShares.push({ character: winners[winner], percentage: country.getVoteShare(winners[winner]) });
        }
        //console.log(winnerShares);
        allCountries[countryID] = winnerShares;
    }

    return allCountries;
}
